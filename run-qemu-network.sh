qemu-system-x86_64 -cpu qemu64,apic,fsgsbase,fxsr,rdrand,rdtscp,xsave,xsaveopt \
    -smp 2 -m 512M \
    -device isa-debug-exit,iobase=0xf4,iosize=0x04 \
    -display none -serial stdio \
    -kernel rusty-loader-x86_64 \
    -netdev user,id=u1,hostfwd=tcp::8000-:8000 \
    -device rtl8139,netdev=u1 \
    -initrd $1

use std::thread;
use std::time::{Duration, Instant};
use tiny_http::{Response, Server, StatusCode};

const NTHREADS: u64 = 4;
const NUM_STEPS: u64 = 10000000;

/// A very inefficient, but also very parallel way to calculate pi.
fn multithreaded_pi() -> (f64, Duration) {
    let step = 1.0 / NUM_STEPS as f64;
    let mut sum = 0.0 as f64;

    let now = Instant::now();

    let threads: Vec<_> = (0..NTHREADS)
        .map(|tid| {
            thread::spawn(move || {
                let mut partial_sum = 0 as f64;
                let start = (NUM_STEPS / NTHREADS) * tid;
                let end = (NUM_STEPS / NTHREADS) * (tid + 1);

                for i in start..end {
                    let x = (i as f64 + 0.5) * step;
                    partial_sum += 4.0 / (1.0 + x * x);
                }

                partial_sum
            })
        })
        .collect();

    for t in threads {
        sum += t.join().unwrap();
    }
    let duration = now.elapsed();
    let pi = sum * (1.0 / NUM_STEPS as f64);
    println!("Time to calculate pi as {}: {:?}", pi, duration);
    (pi, duration)
}

fn main() {
    let server = Server::http("0.0.0.0:8000").unwrap();

    for request in server.incoming_requests() {
        println!(
            "received request! method: {:?}, url: {:?}",
            request.method(),
            request.url(),
        );

        let response = match request.url() {
            "/" => Response::from_string("Hello fellow Unikernel users"),
            "/pi" => {
                let (pi, duration) = multithreaded_pi();
                let response_string =
                    format!("π is almost the value {pi}.\nTook {duration:?} to compute with {NTHREADS} threads.");
                Response::from_string(response_string)
            }
            _ => Response::from_string("404 - not found").with_status_code(StatusCode(404)),
        };
        request.respond(response).unwrap();
    }
}
